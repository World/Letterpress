# Russian translation for the letterpress' package.
# Copyright (C) 2023 the letterpress' COPYRIGHT HOLDER
# This file is distributed under the same license as the letterpress package.
# Fyodor Sobolev, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: letterpress\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-08-15 02:04+0200\n"
"PO-Revision-Date: 2023-09-22 08:18+0000\n"
"Last-Translator: gregorni <gregorniehl@web.de>\n"
"Language-Team: Russian <https://hosted.weblate.org/projects/letterpress/"
"letterpress/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 5.1-dev\n"

#. Translators: Translate as "Letterpress"
#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:4
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:7 src/main.py:161
#: src/gtk/window.blp:5
msgid "Letterpress"
msgstr "Letterpress"

#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:5
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:9
msgid "Create beautiful ASCII art"
msgstr "Создайте красивые ASCII-арты"

#. Translators: These are search terms for the application;
#. the translation should contain the original list AND the translated strings.
#. The list must end with a semicolon.
#: data/io.gitlab.gregorni.ASCIIImages.desktop.in:14
msgid "image;ascii;convert;conversion;text;"
msgstr ""
"image;ascii;convert;conversion;text;изображение;конвертация;конверсия;текст;"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:8 src/main.py:163
msgid "Letterpress Contributors"
msgstr "Разработчики Letterpress"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:12
msgid ""
"Letterpress converts your images into a picture made up of ASCII characters. "
"You can save the output to a file, copy it, and even change its resolution! "
"High-res output can still be viewed comfortably by lowering the zoom factor."
msgstr ""
"Letterpress конвертирует ваши изображения в картинку, состоящую из символов "
"ASCII. Вы можете сохранить результат в файл, копировать, и даже менять его "
"разрешение! Уменьшая масштаб, можно с комфортом увидеть результат в высоком "
"разрешении."

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:29
msgid "image"
msgstr "изображение"

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:31
msgid "ascii"
msgstr "ascii"

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:33
msgid "convert"
msgstr "конвертация"

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:35
msgid "conversion"
msgstr "конверсия"

#. Translators: This is a search term and should be lowercase
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:37
msgid "text"
msgstr "текст"

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:55
msgid "Overview"
msgstr "Обзор"

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:60
msgid "Copy output"
msgstr "Скопировать результат"

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:65
msgid "Save output"
msgstr "Сохранить результат"

#. Translators: This is a screenshot caption
#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:70
msgid "Adjust output width"
msgstr "Изменить ширину вывода"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:93
msgid "Rebrand to Letterpress"
msgstr "Переименовано в Letterpress"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:94
msgid "Replaced icon on welcome screen with illustration"
msgstr "Иконка на приветственном экране заменена на иллюстрацию"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:95
msgid "Changed warning dialog to Adw.MessageDialog"
msgstr "Диалог с предупреждением изменён на Adw.MessageDialog"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:96
msgid "Improved adaptiveness of \"Output Width\" subtitle"
msgstr "Улучшена адаптивность надписи \"Ширина вывода\""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:97
msgid "Swapped buttons for copying and saving output"
msgstr "Кнопки для копирования и сохранения результата поменяны местами"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:98
msgid "Added a string to translations"
msgstr "Добавлена строка для перевода"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:99
msgid "Added Turkish translation"
msgstr "Добавлен турецкий перевод"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:100
msgid "Added Czech translation"
msgstr "Добавлен чешский перевод"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:107
msgid "Added French translation"
msgstr "Добавлен французский перевод"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:108
msgid "Added Russian translation"
msgstr "Добавлен русский перевод"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:109
msgid "Added Occitan translation"
msgstr "Добавлен окситанский перевод"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:110
msgid "Added Italian translation"
msgstr "Добавлен итальянский перевод"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:111
msgid "Fixed \"Open with…\" functionality"
msgstr "Исправлена функция \"Открыть с помощью...\""

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:112
msgid "The app now remembers window size and state"
msgstr "Приложение теперь запоминает размер окна и состояние"

#: data/io.gitlab.gregorni.ASCIIImages.appdata.xml.in:118
msgid "First Release! 🎉"
msgstr "Первый выпуск! 🎉"

#. Translators: Translate this string as your translator credits.
#. Name only:    gregorni
#. Name + URL:   gregorni https://gitlab.com/gregorni/
#. Name + Email: gregorni <gregorniehl@web.de>
#. Do not remove existing names.
#. Names are separated with newlines.
#: src/main.py:173
msgid "translator-credits"
msgstr "Fyodor Sobolev https://github.com/fsobolev"

#: src/main.py:174
msgid "Copyright © 2023 Letterpress Contributors"
msgstr "Copyright © 2023 Разработчики Letterpress"

#: src/main.py:182
msgid "Code and Design borrowed from"
msgstr "Код и дизайн позаимствованы из"

#: src/window.py:88
msgid "Width of the ASCII image in characters"
msgstr "Ширина ASCII изображения в символах"

#. Translators: Do not translate "{basename}"
#: src/window.py:104
#, python-brace-format
msgid "\"{basename}\" is not of a supported image type."
msgstr "\"{basename}\" не является поддерживаемым файлом изображения."

#: src/window.py:197
msgid "The output is too large to be copied."
msgstr "Результат слишком большой, копирование не удалось."

#: src/window.py:199
msgid "Please save it to a file instead or decrease the output width."
msgstr "Пожалуйста, сохраните его в файл или уменьшите ширину."

#: src/window.py:202
msgid "_OK"
msgstr "_Ок"

#: src/window.py:206
msgid "Output copied to clipboard"
msgstr "Результат скопирован в буфер обмена"

#: src/file_chooser.py:37 src/file_chooser.py:104
msgid "Select a file"
msgstr "Выбрать файл"

#: src/file_chooser.py:44
msgid "Supported image files"
msgstr "Поддерживаемые форматы изображений"

#. Translators: Do not translate "{display_name}"
#: src/file_chooser.py:82
#, python-brace-format
msgid "Unable to save \"{display_name}\""
msgstr "Невозможно сохранить \"{display_name}\""

#. Translators: Do not translate "{display_name}"
#: src/file_chooser.py:91
#, python-brace-format
msgid "\"{display_name}\" saved"
msgstr "\"{display_name}\" сохранено"

#: src/file_chooser.py:93
msgid "Open"
msgstr "Открыть"

#: src/gtk/window.blp:16
msgid "Tips"
msgstr "Подсказки"

#: src/gtk/window.blp:24
msgid "Main Menu"
msgstr "Главное меню"

#: src/gtk/window.blp:86
msgid "Create ASCII Art"
msgstr "Создайте ASCII-арт"

#: src/gtk/window.blp:87
msgid "Open or drag and drop an image to generate an ASCII art version of it"
msgstr "Откройте или перетащите изображение для создания из него ASCII-арта"

#: src/gtk/window.blp:92
msgid "Open File…"
msgstr "Открыть файл…"

#: src/gtk/window.blp:111
msgid "Drop Here to Open"
msgstr "Перетащите сюда, чтобы открыть"

#: src/gtk/window.blp:172
msgid "Output Width"
msgstr "Ширина вывода"

#: src/gtk/window.blp:190
msgid "Copy to clipboard"
msgstr "Скопировать в буфер обмена"

#: src/gtk/window.blp:197
msgid "Save to file"
msgstr "Сохранить в файл"

#: src/gtk/window.blp:224
msgid "_Open File…"
msgstr "_Открыть файл…"

#: src/gtk/window.blp:231
msgid "_Keyboard Shortcuts"
msgstr "_Комбинации клавиш"

#: src/gtk/window.blp:236
msgid "_About Letterpress"
msgstr "_О приложении"

#: src/gtk/zoom-box.blp:12
msgid "Zoom out"
msgstr "Отдалить"

#: src/gtk/zoom-box.blp:28
msgid "Reset zoom"
msgstr "Сбросить масштаб"

#: src/gtk/zoom-box.blp:40
msgid "Zoom in"
msgstr "Приблизить"

#: src/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Основное"

#: src/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Open File"
msgstr "Открыть файл"

#: src/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Показать комбинации клавиш"

#: src/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Quit"
msgstr "Выход"

#: src/gtk/help-overlay.blp:30
msgctxt "shortcut window"
msgid "Text View"
msgstr "Отображение текста"

#: src/gtk/help-overlay.blp:33
msgctxt "shortcut window"
msgid "Increase Output Width"
msgstr "Увеличить ширину вывода"

#: src/gtk/help-overlay.blp:38
msgctxt "shortcut window"
msgid "Decrease Output Width"
msgstr "Уменьшить ширину вывода"

#: src/gtk/help-overlay.blp:43
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Приблизить"

#: src/gtk/help-overlay.blp:48
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Отдалить"

#~ msgid "Convert images into ASCII characters"
#~ msgstr "Конвертируйте изображения в символы ASCII"

#~ msgid "Turn images into text"
#~ msgstr "Превратите изображения в текст"

#~ msgid "Drag and drop images here"
#~ msgstr "Перетащите изображение сюда"

#~ msgid "_Open…"
#~ msgstr "_Открыть..."
